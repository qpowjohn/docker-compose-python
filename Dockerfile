FROM python:3.8-slim-buster

# 安裝必要的套件
RUN apt-get update

# 安裝 Python 套件
RUN pip install Flask pymysql

# 設定工作目錄
WORKDIR /app

# 複製應用程式程式碼
COPY main.py /app/


# 設定環境變數
ENV FLASK_APP main.py

# 開放連接埠
EXPOSE 5000

# 啟動應用程式和資料庫服務
CMD flask run --host=0.0.0.0
