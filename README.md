# 透過docker-compose部屬python

## attention: this is side project!

## 使用情境

How to use:
http://localhost:5000/Batch

<details>
 <summary><code>GET</code> <code><b>/</b></code> <code>查詢</code></summary>

##### Parameters

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | SSN      |  required | string   | 身分證號  |

</details>

<details>
 <summary><code>PUT</code> <code><b>/</b></code> <code>新增</code></summary>

##### Parameters

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | SSN      |  required | string   | 身分證號  |
> | Name      |  required | string   | 名字  |


</details>
<details>
<summary><code>POST</code> <code><b>/</b></code> <code>修改</code></summary>

##### Parameters

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | SSN      |  required | string   | 身分證號  |
> | Name      |  required | string   | 名字  |

</details>
<details>

 <summary><code>DELETE</code> <code><b>/</b></code> <code>刪除</code></summary>

##### Parameters

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | SSN      |  required | string   | 身分證號  |

</details>


## 部屬

```
docker-compose -f docker-compose.yml up
```

## 