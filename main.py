from flask import Flask, request, jsonify
import pymysql
import os

app = Flask(__name__)

# 資料庫連接設定
db = pymysql.connect(
    host=os.environ["DATABASE_HOST"],
    user=os.environ["DATABASE_USER"],
    password=os.environ["DATABASE_PASSWORD"],
    database=os.environ["DATABASE_NAME"]
)

# 建立資料表
with db.cursor() as cursor:
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS IDCard (
            SN int NOT NULL AUTO_INCREMENT,
            SSN varchar(10) NOT NULL UNIQUE,
            Name nvarchar(100) NOT NULL,
            PRIMARY KEY (SN)
        )
    """)
    db.commit()

def is_valid_taiwan_id(id: str) -> bool:
    if len(id) != 10:
        return False
    alphaTable = {'A': 1, 'B': 10, 'C': 19, 'D': 28, 'E': 37, 'F': 46,
            'G': 55, 'H': 64, 'I': 39, 'J': 73, 'K': 82, 'L': 2, 'M': 11,
            'N': 20, 'O': 48, 'P': 29, 'Q': 38, 'R': 47, 'S': 56, 'T': 65,
            'U': 74, 'V': 83, 'W': 21, 'X': 3, 'Y': 12, 'Z': 30}
    sum = alphaTable[id[0]] + int(id[1]) * 8 + int(id[2]) * 7 + int(id[3]) * 6 + int(id[4]) * 5 + int(id[5]) * 4 + int(id[6]) * 3 + int(id[7]) * 2 + int(id[8]) * 1 + int(id[9])

    return sum % 10 == 0

@app.route("/Batch", methods=["GET", "POST", "PUT", "DELETE"])
def batch():
    ssn = request.args.get("SSN")
    name = request.args.get("Name")
    
    if request.method == "GET":
        with db.cursor() as cursor:
            cursor.execute("SELECT Name FROM IDCard WHERE SSN=%s", (ssn,))
            result = cursor.fetchone()
            if result:
                return jsonify({"Name": result[0]})
            else:
                return jsonify({"error": "SSN not found"})
    
    elif request.method == "POST":
        if not is_valid_taiwan_id(ssn):
            return jsonify({"error": "Invalid SSN"})
        with db.cursor() as cursor:
            try:
                cursor.execute("INSERT INTO IDCard (SSN, Name) VALUES (%s, %s)", (ssn, name))
                db.commit()
                return jsonify({"action": "inserted"})
            except pymysql.err.IntegrityError:
                return jsonify({"error": "SSN already exists, please retry"})
    
    elif request.method == "PUT":
        with db.cursor() as cursor:
            cursor.execute("UPDATE IDCard SET Name=%s WHERE SSN=%s", (name, ssn))
            db.commit()
            if cursor.rowcount > 0:
                return jsonify({"action": "updated"})
            else:
                return jsonify({"error": "SSN not found"})
    
    elif request.method == "DELETE":
        with db.cursor() as cursor:
            cursor.execute("DELETE FROM IDCard WHERE SSN=%s", (ssn,))
            db.commit()
            if cursor.rowcount > 0:
                return jsonify({"action": "deleted"})
            else:
                return jsonify({"error": "SSN not found"})

if __name__ == "__main__":
    app.run()

